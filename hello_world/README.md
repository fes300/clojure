```
** to build:
$ java -cp 'cljs.jar:lib/*:src' clojure.main build.clj

** to watch with REPL:
$ rlwrap java -cp 'cljs.jar:lib/*:src' clojure.main repl.clj

** to keep an eye on the build logs:
$ tail -f out/watch.log

```
build and repl commands add all the dependencies in the lib folder.
repl command make use of lrwrap to make the repl commandline more 'human'...



(ns hello-world.core
  (:require cljsjs.react [clojure.browser.repl :as repl]))

(defonce conn
    (repl/connect "http://localhost:9000/repl"))

(enable-console-print!)

(println "Hello react!")


;; multiply function
(defn multiply [a b]
    (* a b))
